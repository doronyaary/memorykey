# ![](memorykey1.png) &nbsp;&nbsp; MemoryKey - Super easy key/value database

MemoryKey is an in-memory key/value database that is super easy to maintain and operate. The sole purpose is to create a cloud-ready key/value database which supprts the 99% majority of the use cases (which are GET/SET/DEL operations).

## Getting started

The current release contains the binary distribution (and the python code also) so there are no compiling involved - just several pip installs. Follow these installation steps (tested on RockyLinux 8/9):

```bash
dnf install python3 python3-pip
pip3 install psutil
pip3 install sqlite3
pip3 install bottle
```

Once you installed these requirements you are ready to use MemoryKey.

## Usage Notes

You can actually just run the executable within the bin folder and MemoryKey will run listening to POST requests on port 8080 with binding to 127.0.0.1 only having a hard limit of 256MB (read about memory limit below). The executable accepts the following command line arguments:

| Argument | Notes |
| - | - |
| --bind-host | The binding of the process. The default is 127.0.0.1. Use IP address to bind to (example: 0.0.0.0 for all). |
| --bind-port | The port number to use. The default is 8080. |
| --max-memory-mb | The maximum amount of megabytes to allow the process to use. |
| --silent | Makes MemoryKey to be completely silent and work without any output. |
| --api-key | Defines the API key that the client would have to send in order to use the endpoints. |

## Memory Usage

Given that MemoryKey is an in-memory database, some form of memory limitation must apply otherwise there is a good chance that the memory usage will overload the system. You can limit the memory usage by defining the maximum megabytes to use. Once this limit is reached MemoryKey will not perform writes anymore but it will allow reads. In other words, you will be able to perform GET/DEL operations but SET will be limitted. The default is 256MB which is super low for production usage. We recommend setting it to the operating system limit reducing at least 1GB of RAM for the operating system itself.

## Usage Examples 

These are few scenaries and examples on how to start MemoryKey:

**Starting with default values:**

```bash
./memorykey 
```

**Starting with IP binding:**

```bash
./memorykey --bind-host=0.0.0.0
```

**Starting with IP and another port number and memory limits:**

```bash
./memorykey --bind-host=0.0.0.0 --bind-port=9001 --max-memory-mb=1024
```

## Endpoints 

Once executed, MemoryKey will expose the following endpoints:

| Endpoint | Purpose | Since (Version) |
| - | - | - |
| /get | Used to retreive a value by its key | 1.4.0 |
| /set | Used to set a value by its key | 1.4.0 |
| /del | Used to delete a key | 1.4.0 |

MemoryKey will answer to POST requests (and not GET requests). In addition, the content type is 'text/json'.

## Sending Requests

Any application can send requests with the simplest HTTP request library. The following are some examples for a common programming languages. Please keep in mind that the return content type is 'text/json'.

**Using Python**

```python 
import requests 
mydata = {'apikey':'1234567','key':'mykeyname'}
x = requests.post('http://YOURIP:8080/get',mydata)
```

**Using Bash**

```bash
curl -X POST -d 'apikey=1234567' -d 'key=mykeyname' http://YOURIP:8080/get
```

## Future Development

The following subject(s) are currently developed:

- Master/Slave replication
- Support for both GET & POST request types (according to user parameters)
- Implementing 'mkctl' as a standalone executable to remotely control and admin MemoryKey instances

## Releases 

**1.4.0**
Realsed. Initial release with binaries included.

**1.5.0**
Upcomming. Will include the following changes:

- Maximum memory default increased from 256MB to 512MB
- Added python3 requirement (asyncio) as part of preparation for replication 
- API key will become mandatory when replication is used 

