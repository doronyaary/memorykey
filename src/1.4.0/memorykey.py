import os,sys,psutil,time,threading,sqlite3,requests
from bottle import route,run,template,request,response

SYS_VERSION = "1.4.0"
SYS_MAX_MEM_MB=256
SYS_API_BIND="127.0.0.1"
SYS_API_PORT=8080
SYS_API_KEY=""
SYS_CON=None
SYS_HALT=0
SYS_SILENT=0

@route('/get', method="POST")
def index():
    global SYS_CON
    global SYS_API_KEY
    global SYS_HALT
    response.status = 200
    response.content_type = 'text/json'
    response.headers['Server'] = 'MemoryKey'
    response.headers['Version'] = str(SYS_VERSION)
    if (SYS_API_KEY != ''):
        if (request.forms.get('apikey') is None):
            return '{"state":"failed","info":"not authorized"}'
    if (request.forms.get('key') is None):
        return '{"state":"failed","info":"key name is not provided with the request body"}'
    if (request.forms.get('key') == ''):
        return '{"state":"failed","info":"the key is empty"}'
    rs = SYS_CON.cursor()
    rs.execute("select count(*) from keys where key_name = '" + str(request.forms.get('key')) + "';")
    row = rs.fetchone()
    tmpchk = int(row[0])
    if (tmpchk == 0):
        rs = None
        return '{"state":"failed","info":"key does not exist"}'
    rs.execute("select key_value from keys where key_name = '" + str(request.forms.get('key')) + "';")
    row = rs.fetchone()
    return '{"state":"success","value":"' + str(row[0]) + '"}'

@route('/del', method="POST")
def index():
    global SYS_CON
    global SYS_API_KEY
    global SYS_HALT
    response.status = 200
    response.content_type = 'text/json'
    response.headers['Server'] = 'MemoryKey'
    response.headers['Version'] = str(SYS_VERSION)
    if (SYS_API_KEY != ''):
        if (request.forms.get('apikey') is None):
            return '{"state":"failed","info":"not authorized"}'
    if (request.forms.get('key') is None):
        return '{"state":"failed","info":"key name is not provided with the request body"}'
    if (request.forms.get('key') == ''):
        return '{"state":"failed","info":"the key is empty"}'
    rs = SYS_CON.cursor()
    rs.execute("select count(*) from keys where key_name = '" + str(request.forms.get('key')) + "';")
    row = rs.fetchone()
    tmpchk = int(row[0])
    if (tmpchk == 0):
        rs = None
        return '{"state":"failed","info":"key does not exist"}'
    rs.execute("select key_value from keys where key_name = '" + str(request.forms.get('key')) + "';")
    row = rs.fetchone()
    return '{"state":"success","value":"' + str(row[0]) + '"}'

@route('/set', method="POST")
def index():
    global SYS_CON
    global SYS_API_KEY
    global SYS_HALT
    response.status = 200
    response.content_type = 'text/json'
    response.headers['Server'] = 'MemoryKey'
    response.headers['Version'] = str(SYS_VERSION)
    if (SYS_API_KEY != ''):
        if (request.forms.get('apikey') is None):
            return '{"state":"failed","info":"not authorized"}'
    if (SYS_HALT == 1):
        return '{"state":"failed","info":"memory limit reached"}'
    if (request.forms.get('key') is None):
        return '{"state":"failed","info":"key name is not provided with the request body"}'
    if (request.forms.get('key') == ''):
        return '{"state":"failed","info":"the key is empty"}'
    if (request.forms.get('value') is None):
        return '{"state":"failed","info":"value is not provided with the request body"}'
    if (request.forms.get('value') == ''):
        return '{"state":"failed","info":"the value is empty"}'
    rs = SYS_CON.cursor()
    rs.execute("select count(*) from keys where key_name = '" + str(request.forms.get('key')) + "';")
    row = rs.fetchone()
    tmpchk = int(row[0])
    if (tmpchk == 0):
        rs.execute("insert into keys values ('" + str(request.forms.get('key')) + "','" + str(request.forms.get('value')) + "');")
        SYS_CON.commit()
    if (tmpchk > 0):
        rs.execute("update keys set key_value = '" + str(request.forms.get('value')) + "' where key_name = '" + str(request.forms.get('key')) + "';")
        SYS_CON.commit()
    rs = None
    return '{"state":"success","info":"debug1"}'

def header():
    global SYS_VERSION
    global SYS_SILENT
    if (SYS_SILENT == 0):
        print("")
        print(" MemoryKey (" + str(SYS_VERSION) + ") - In-Memory Key-Value Database")
        print(" Released under the MIT license with no warrenty.")
    return

def err(errSeverity,errText,exitAfter):
    global SYS_SILENT
    if (SYS_SILENT == 0):
        print(" [" + str(errSeverity).upper() + "]: " + str(errText))
    if (exitAfter == 1):
        if (SYS_SILENT == 0):
            print("")
        sys.exit(0)

def iterate_arguments():
    global SYS_MAX_MEM_MB
    global SYS_API_BIND
    global SYS_API_PORT
    global SYS_API_KEY
    for ar in sys.argv:
        if (ar.lower().startswith("-max-memory-mb=")):
            SYS_MAX_MEM_MB=ar.replace("--max-memory-mb=","")
        if (ar.lower().startswith("--bind-host=")):
            SYS_API_BIND=ar.replace("--bind-host=","")
        if (ar.lower().startswith("--bind-port=")):
            SYS_API_PORT=ar.replace("--bind-port=","")
        if (ar.lower().startswith("--api-key=")):
            SYS_API_KEY=ar.replace("--api-key=","")

def validate_arguments():
    global SYS_MAX_MEM_MB
    global SYS_API_BIND
    global SYS_API_PORT
    if (str(SYS_MAX_MEM_MB).isdigit() == False):
        err('E','Invalid memory definition. Exiting.',1)
    return

def memory_watcher():
    global SYS_MAX_MEM_MB
    global SYS_HALT
    time.sleep(5)
    chk = int(0)
    while (True):
        process = psutil.Process()
        chk = int(process.memory_info().rss/1024/1024)
        if (chk > SYS_MAX_MEM_MB):
            SYS_HALT = 1
        if (chk < SYS_MAX_MEM_MB):
            SYS_HALT = 0
        time.sleep(10)

if __name__ == '__main__':
    for ar in sys.argv:
        if (ar.lower() == '--silent'):
            SYS_SILENT=1
    header()
    SYS_CON=sqlite3.connect(':memory:')
    rs = SYS_CON.cursor()
    rs.execute("create table keys (key_name text primary key, key_value text not null);")
    SYS_CON.commit()
    iterate_arguments()
    err('I', 'Validating arguments...', 0)
    validate_arguments()
    if (int(SYS_MAX_MEM_MB) == 0):
        err('W','MemoryKey is running without any memory restriction - as specifically requested by the user.',0)
    if (int(SYS_MAX_MEM_MB) > 0):
        err('I', 'Starting memory watcher for [' + str(SYS_MAX_MEM_MB) + '] MB...', 0)
        tmem = threading.Thread(target=memory_watcher)
        tmem.start()
    err('I','Starting API listener (' + str(SYS_API_BIND) + ':' + str(SYS_API_PORT) + ')...',0)
    run(host=str(SYS_API_BIND),port=int(SYS_API_PORT), quiet=True)
